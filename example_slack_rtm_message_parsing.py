import logging

from hein_utilities.slack_integration.bots import WebClientOverride
from hein_utilities.slack_integration.slack_managers import RTMControlManager


# set up a console logger that logs only to the console, and a slack logger that will log to both the console and the
# slack channel
console_logger = logging.getLogger('console logger')
slack_logger = logging.getLogger('slack logger')
# set the logging level for the console logger and the slack logger
logging.basicConfig(level=logging.INFO)

# the slack Bot User OAuth token. Starts with 'xoxb-'
bot_token = 'xoxb-...'
# name of the slack channel; must start with '#'
channel_name = '#...'
# channel id to listen for events on, for example: C1234ABCD. Find more information on finding this here:
# https://stackoverflow.com/questions/40940327/what-is-the-simplest-way-to-find-a-slack-team-id-and-a-channel-id
channel_id = '...'


# instantiate a control manager
slack_manager = RTMControlManager(
    token=bot_token,
    channel_name=channel_name,
    channel_id=channel_id,
)


# set up message parsing to run whenever a message is sent to the channel the slack_manager was set to listen on
@slack_manager.run_on(event='message')
def catch_message(**payload):
    web_client = payload['web_client']
    try:
        # if a message in the slack channel exactly matches (case sensitive) SOME_COMMAND and the message was not
        # sent by a bot
        if slack_manager.catch_and_match(SOME_COMMAND, **payload):
            with WebClientOverride(slack_manager, web_client):
                do_something()
                console_logger.info('did something - will be sent to console only')
                slack_logger.info('did something')  # this will be sent to the slack channel and the console
    except Exception as e:
        # if something went wrong trying to handle a message (such as an error thrown in do_something()) this will be
        # caught here
        message = payload['data']
        text = str(message.get('text'))
        console_logger.info(f'slack command {text} fail')
        # this sends the error message to the console and the slack channel
        slack_logger.error(f'slack command {text} fail\n'
                           f'Error: {e}')


def do_something():
    print('do something')


if __name__ == '__main__':
    SOME_COMMAND = 'text you want to look out for in the slack channel'

    # function to overwrite the help_query() method of the slack_manager
    def help_query():
        help = 'SOME_COMMAND - description of what SOME_COMMAND will do in the Python script'
        return help

    # set the response that the slack app will send to the channel when the word 'help' is sent to the slack channel
    slack_manager.help_query = help_query

    # if logging INFO or more important with the slack_logger, the log statement will be sent by slack bot to slack
    # channel
    slack_logger.addHandler(slack_manager)
    slack_logger.info('set up slack logging')

    # you can also send direct messages from the slack app to the channel like this
    slack_manager.post_slack_message('posting a test slack message')

    # choose one of the two options to start the real time messaging client
    # ------------------------------------------------------------------------------------------------------------------
    # 1
    # # start slack real time messaging client in a separate thread - non-blocking so you can have other code run
    # after this
    import threading
    threading.Thread(target=slack_manager.start_rtm_client).start()
    # ------------------------------------------------------------------------------------------------------------------
    # 2
    # to start slack real time messaging client in the main thread, no code after this will run (this is blocking)
    slack_manager.start_rtm_client()
    # ------------------------------------------------------------------------------------------------------------------


