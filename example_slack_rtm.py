from hein_utilities.slack_integration.bots import RTMSlackBot, WebClientOverride

# instantiate the slack bot with the appropriate token and settings
rtm_bot = RTMSlackBot(...)


# create event catch functions and add them to the RTM manager via .run_on
@rtm_bot.run_on(event='message')
def catch_message(**payload):
    """catches and interprets a message"""
    text = payload['text']
    ...  # parse text and perform operations

    # messages can be sent directly using the bot
    rtm_bot.post_slack_message(
        'this message will be sent, but only after the entire catch message function has completed'
    )

    # if the code block in the message is long running and you need immediate feedback, use this context manager
    #   The WebClientOverride context manager facilitates temporary override of the web client of the bot instance,
    #   instead using the web client provided by the RTM event
    web_client = payload['web_client']
    with WebClientOverride(rtm_bot, web_client):
        rtm_bot.post_slack_message(
            'This message will be sent immediately to the Slack channel'
        )


# once everything is set up, the rtm client needs to be started (this is the unix-safe way of doing this)
rtm_bot.start_rtm_client()

