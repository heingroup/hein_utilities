"""
There are several standard methods that we use for message parsing of RTM messages.

e.g.
- ignoring bot users
- ignoring events
- only executing methods when the user is authorized
"""
from hein_utilities.slack_integration.bots import RTMSlackBot
from hein_utilities.slack_integration.parsing import ignore_bot_users, ignore_events, check_authorized, standard_parsing


bot = RTMSlackBot(...)


@bot.run_on(event='message')
@ignore_bot_users
def non_bots(**payload):
    bot.post_slack_message('this method will only be triggered by non-bots')


@bot.run_on(event='message')
@ignore_events
def non_events(**payload):
    bot.post_slack_message('this method will not be triggered by events')


# since both are frequently needed together, the 'standard_parsing' method does both
@bot.run_on(event='message')
@standard_parsing
def both_options(**payload):
    bot.post_slack_message('this event will be triggered on non-events and by non-bot users')


@bot.run_on(event='message')
@check_authorized(['userid1', 'userid2', ...])
def authorized_access_only(**payload):
    bot.post_slack_message('this event will only be triggered by authorized users')
