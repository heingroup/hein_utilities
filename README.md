# Hein Utilities

A collection of utility classes and methods which are used in a variety of Hein projects, but have no rightful home. 
Should any of these utilities be dedicated to a given home, refactor warnings will be used to ensure backwards compatibility. 
