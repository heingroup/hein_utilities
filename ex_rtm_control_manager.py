"""example script illustrating how to create a slack RTM experiment control manager"""
from hein_utilities.slack_integration.slack_managers import RTMControlManager


def start_thing():
    """this method will be executed on a start trigger"""
    print('starting!')


def stop():
    """this method will be executed on a stop trigger"""
    print('stopping')


def resume():
    """this method will be executed on a resume trigger"""
    print('resuming')


def pause():
    """this method will be executed on a pause trigger"""
    print('pausing')


def status():
    """this method will return a string which represents the status of the system being controlled"""
    return 'OVER 9000!'


# create manager class
manager = RTMControlManager(
    token=...,
    channel_name=...,
    # register action methods
    start_action=start_thing,
    stop_action=stop,
    resume_action=resume,
    pause_action=pause,
    status_query=status,
)

# notify the user that it's all ready
manager.post_slack_message('ready and awaiting orders!')

# start the RTM client
manager.start_rtm_client()
