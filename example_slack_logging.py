"""An example file illustrating how to enable logging to a Slack bot"""
import logging
from hein_utilities.slack_integration.bots import SlackBot

# logging must be configured to enable pass through
logging.basicConfig(
    level=logging.INFO,
)
logger = logging.getLogger('example Slack logger')

slack_handler = SlackBot(
    token=...,
    channel_name=...,
)

# add the slack handler to the logger
logger.addHandler(slack_handler)

logger.info('test message which will be posted to the Slack channel')

"""
To enable Slack passthrough of other logging messages (e.g. info/debug messages from the loggers of imported modules, 
import the logger from that module, then add the slack handler to that logger in the same way. 
"""

