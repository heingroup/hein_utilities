"""
The RTMSlackBot may be subclassed to have similar functionality as decorating, but enables difference workflows
(e.g. when the class needs to be fully instantiated and there is no opportunity for the scripter to decorate)
"""

from hein_utilities.slack_integration.bots import RTMSlackBot


# subclass and define parsing methods
class ApplicationSpecificRTMBot(RTMSlackBot):
    # error parsing method
    def run_on_error(self, **payload):
        text = payload['text']
        ...

    # message parsing method
    def run_on_message(self, **payload):
        text = payload['text']
        ...  # parse text and perform operations


# instantiate as you would the RTMSlackBot
app_bot = ApplicationSpecificRTMBot(...)

